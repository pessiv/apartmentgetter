def getTheList():
    lista = []
    with open("./distances.txt") as f:
        for line in f:
            lista.append(line)
    return lista
def getSmallestValue(lista):
    smallestValue = 99999999999
    for n in lista:
        if int(n.split("#")[2]) <= smallestValue:
            smallestValue = int(n.split("#")[2])
    return smallestValue
def orderTheList():
    orderedList = []
    listToBeOrdered = getTheList()
    while len(listToBeOrdered) > 0:
        sv = getSmallestValue(listToBeOrdered)
        for n in listToBeOrdered:
            if int(n.split("#")[2]) == sv:
                orderedList.append(n)
                listToBeOrdered.remove(n)
    return orderedList
def printOrderedList(ol):
    for n in ol:
        print(n)