import googlemaps
from datetime import datetime
gmaps = googlemaps.Client(key='Insert your api key here')
hkiStations = ["Koivusaari metro","Lauttasaari metro","Ruoholahti metro","Rautatientori metro","Helsingin yliopisto metro","Hakaniemi metro","Sörnäinen metro","Kalasatama metro","Kulosaari metro","Herttoniemi metro","Siilitie metro","Itäkeskus metro","Myllypuro metro","Kontula metro","Mellunmäki metro","Puotila metro","Rastila metro","Vuosaari metro","Pasilan rautatieasema","Helsingin rautatieasema"]
esbStations = ["Matinkylä metro","Niittykumpu metro","Urheilupuisto metro","Tapiola metro","Aalto-yliopisto metro","Keilaniemi metro"]
vanStations = ["Tikkurila rautatieasema"]
def loadAdresses():
    addresses = []
    with open("./addresslist2.txt") as f:
        for line in f:
            addresses.append(line)
    return addresses
def goThroughAddresses(list):
    amountOfRequests = len(hkiStations)+len(esbStations)+len(vanStations)+290
    for address in list:
        if "helsinki" in address:
            for station in hkiStations:
                directions_result = gmaps.distance_matrix(address,station,mode="walking")["rows"][0]["elements"][0]["distance"]["value"]
                print(address+"#"+station+"#"+directions_result)
                amountOfRequests+=1
        elif "espoo" in address:
            for station in esbStations:
                directions_result = gmaps.distance_matrix(address,station,mode="walking")["rows"][0]["elements"][0]["distance"]["value"]
                print(address+"#"+station+"#"+directions_result)
                amountOfRequests+=1
        elif "vantaa" in address:
            for station in vanStations:
                directions_result = gmaps.distance_matrix(address,station,mode="walking")["rows"][0]["elements"][0]["distance"]["value"]
                print(address+"#"+station+"#"+directions_result)
                amountOfRequests+=1
    print(amountOfRequests*0.005)
#goThroughAddresses(loadAdresses())
print(gmaps.distance_matrix("Espoo", "Oulu", mode='walking')["rows"][0]["elements"][0]["distance"]["value"])