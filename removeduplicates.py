def removeDuplicatesForGoogleMaps():
    duplicatesRemoved = []
    with open("./addresslist.txt") as f:
        for line in f:
            l1 = line.split(" ")
            toBeChecked = l1
            if toBeChecked not in duplicatesRemoved:
                duplicatesRemoved.append(toBeChecked)
    dr2 = []
    for n in duplicatesRemoved:
        if len(dr2) == 0:
            dr2.append(n)
        else:
            count = 0
            for a in dr2:
                if a[0] == n[0] and a[1] == n[1] and a[-1] == n[-1]:
                    count+=1
            if count == 0:
                dr2.append(n)
    return dr2
def printDuplicatesRemoved():
    for n in removeDuplicatesForGoogleMaps():
        stringx = ""
        for m in n:
            stringx += m + " "
        print(stringx.strip())
printDuplicatesRemoved()