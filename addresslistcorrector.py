def getCityFromTheLink(link):
    l1 = link.split("myytavat-asunnot/")[1]
    return l1.split("/")[0]
def getAddressAndCityInfo():
    addressAndCityInfo = []
    with open("./apartmentcsv.txt") as f:
        for line in f:
            aaci = line.split(";")
            aaci2 = aaci[0]+"; "+getCityFromTheLink(aaci[-1])
            addressAndCityInfo.append(aaci2)
    return addressAndCityInfo
def printInformations():
    for n in getAddressAndCityInfo():
        print(n)
printInformations()